// implementation 2019-01-28
module half_adder(input x, input y, output s, output c);
	assign s = x^y;
	assign c = x&y;
endmodule

module full_adder (input cin, input xin, input yin, output sum, output carry);
	wire s1;
	wire c1;
	wire c2;
	half_adder first(xin, yin, s1, c1);
	half_adder second(cin, s1, sum, c2);
	assign carry = c1|c2;
endmodule

module adder(input [31:0] a, input [31:0] b, input carry_in, output [31:0] c);
	wire [32:0] partial_sum;
	assign partial_sum[0] = carry_in;
	
	generate
        genvar i;
        for (i=0; i<32; i = i + 1) begin: full_adders_block
			full_adder(partial_sum[i], a[i], b[i], c[i], partial_sum[i+1]);
    	end
    endgenerate
endmodule
