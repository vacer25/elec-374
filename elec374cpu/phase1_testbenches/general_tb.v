`timescale 1ns/100ps
module phase1_tb();

parameter op_add = 5'b00011;
parameter op_sub = 5'b00100;
parameter op_and = 5'b00101;
parameter op_or = 5'b00110;
parameter op_shr = 5'b00111;
parameter op_shra = 5'b01000;
parameter op_shl = 5'b01001;
parameter op_ror = 5'b01010;
parameter op_rol = 5'b01011;
parameter op_mul = 5'b01111;
parameter op_div = 5'b10000;
parameter op_neg = 5'b10001;
parameter op_not = 5'b10010;

// Change below to modify instruction
reg [4:0] IR_op = op_not;
reg [3:0] IR_a = 4'd0, IR_b = 4'd1, IR_c = 4'd4;
reg [31:0] ra_val = 32'h31415926, rb_val = 32'hfaceface, rc_val = 32'habcdef12;

// You should not have to modify anything below here for different instructions

wire [31:0] IR = {IR_op, IR_a, IR_b, IR_c, 15'b0};

wire IR_add  = (IR[31:27] == op_add);
wire IR_sub  = (IR[31:27] == op_sub);
wire IR_and  = (IR[31:27] == op_and);
wire IR_or   = (IR[31:27] == op_or);
wire IR_shr  = (IR[31:27] == op_shr);
wire IR_shra = (IR[31:27] == op_shra);
wire IR_shl  = (IR[31:27] == op_shl);
wire IR_ror  = (IR[31:27] == op_ror);
wire IR_rol  = (IR[31:27] == op_rol);
wire IR_mul  = (IR[31:27] == op_mul);
wire IR_div  = (IR[31:27] == op_div);
wire IR_neg  = (IR[31:27] == op_neg);
wire IR_not  = (IR[31:27] == op_not);

reg clk = 0, reset = 0;

reg ctl_PC_in = 0, ctl_PC_out = 0;
reg ctl_IR_in = 0;
reg ctl_GP_in = 0, ctl_GP_out = 0;
reg [3:0] ctl_GP_sel = 0;
reg ctl_HILO_in = 0, ctl_HI_out = 0, ctl_LO_out = 0;

reg ctl_Y_in = 0, ctl_Y_out = 0;
reg ctl_Z_in = 0, ctl_Z_out = 0;
reg ctl_MAR_in = 0;
reg ctl_MDR_in = 0, ctl_MDR_out = 0;

reg ctl_mem_read = 0;
reg ctl_alu_and = 0, ctl_alu_or = 0, ctl_alu_shr = 0, ctl_alu_shra = 0, ctl_alu_shl = 0, ctl_alu_ror = 0, ctl_alu_rol = 0;
reg ctl_alu_neg = 0, ctl_alu_not = 0, ctl_alu_add = 0, ctl_alu_sub = 0, ctl_alu_mul = 0, ctl_alu_div = 0, ctl_alu_incpc = 0;
reg [31:0] mem_data_in = 0;

wire [31:0] mem_data_out;

datapath dut (
    .clk(clk), .reset(reset),

    .ctl_PC_in(ctl_PC_in), .ctl_PC_out(ctl_PC_out),
    .ctl_IR_in(ctl_IR_in),
    .ctl_GP_in(ctl_GP_in), .ctl_GP_out(ctl_GP_out), .ctl_GP_sel(ctl_GP_sel),
    .ctl_HILO_in(ctl_HILO_in), .ctl_HI_out(ctl_HI_out), .ctl_LO_out(ctl_LO_out),

    .ctl_Y_in(ctl_Y_in), .ctl_Y_out(ctl_Y_out),
    .ctl_Z_in(ctl_Z_in), .ctl_Z_out(ctl_Z_out),
    .ctl_MAR_in(ctl_MAR_in),
    .ctl_MDR_in(ctl_MDR_in), .ctl_MDR_out(ctl_MDR_out),
    
    .ctl_mem_read(ctl_mem_read),
    .ctl_alu_and(ctl_alu_and), .ctl_alu_or(ctl_alu_or), .ctl_alu_shr(ctl_alu_shr), .ctl_alu_shra(ctl_alu_shra), .ctl_alu_shl(ctl_alu_shl), .ctl_alu_ror(ctl_alu_ror), .ctl_alu_rol(ctl_alu_rol),
    .ctl_alu_neg(ctl_alu_neg), .ctl_alu_not(ctl_alu_not), .ctl_alu_add(ctl_alu_add), .ctl_alu_sub(ctl_alu_sub), .ctl_alu_mul(ctl_alu_mul), .ctl_alu_div(ctl_alu_div), .ctl_alu_incpc(ctl_alu_incpc),
    .mem_data_in(mem_data_in), .mem_data_out(mem_data_out)
);

parameter Default = 4'b0000, Reg_load1a = 4'b0001, Reg_load1b = 4'b0010, Final_state = 4'b0011, 
                            Reg_load2b = 4'b0100, Print_state = 4'b0101, Reg_load3b = 4'b0110, T0 = 4'b0111, 
                            T1 = 4'b1000, T2 = 4'b1001, T3 = 4'b1010, T4 = 4'b1011, T5 = 4'd12, Reg_check1 = 4'd13, Reg_check2 = 4'd14, Reg_check3 = 4'd15;
reg [3:0] Present_state = Default;

parameter clk_period = 10;

reg [63:0] arithmetic_out;// = 64'b0;
reg [31:0] expected_value;

initial begin
    clk = 0;
    forever #(clk_period/2) clk = !clk;
end
// finite state machine;if clock rising-edge
always @(posedge clk) begin
    case (Present_state)
        Default:    Present_state = Reg_load1a;
        Reg_load1a: Present_state = Reg_load1b;
        Reg_load1b: Present_state = Reg_load2b;
        Reg_load2b: Present_state = Reg_load3b;
        Reg_load3b: Present_state = T0;
        T0:         Present_state = T1;
        T1:         Present_state = T2;
        T2:         Present_state = T3;
        T3:         Present_state = T4;
        T4:         Present_state = T5;
        T5:         Present_state = Reg_check1;
        Reg_check1: Present_state = Reg_check2;
        Reg_check2: Present_state = Reg_check3;
        Reg_check3: Present_state = Final_state;
    endcase
end

always @(*) reset = Present_state == Default;

always @(*) begin
    case (IR_op)
        op_add: arithmetic_out = ($signed(rb_val) + $signed(rc_val));
        op_sub: arithmetic_out = ($signed(rb_val) - $signed(rc_val));
        op_and: arithmetic_out = ($signed(rb_val) & $signed(rc_val));
        op_or:  arithmetic_out = ($signed(rb_val) | $signed(rc_val));
        op_shr: arithmetic_out = (       (rb_val) >> (rc_val % 32));
        op_shra:arithmetic_out = ($signed(rb_val) >> (rc_val % 32));
        op_shl: arithmetic_out = (       (rb_val) << (rc_val % 32));
        op_ror: arithmetic_out = ((rb_val) >> (rc_val % 32) | rb_val << (32 - (rc_val % 32)));
        op_rol: arithmetic_out = ((rb_val) << (rc_val % 32) | rb_val >> (32 - (rc_val % 32)));
        
        op_mul: arithmetic_out = ($signed(ra_val) * $signed(rb_val));
        op_div: arithmetic_out = {($signed(ra_val) / $signed(rb_val)), ($signed(ra_val) % $signed(rb_val))};
        
        op_neg: arithmetic_out = -rb_val;
        op_not: arithmetic_out = ~rb_val;
    endcase
end

always @(posedge clk) begin
    reset <= 0;
    ctl_PC_in <= 0; ctl_PC_out <= 0;
    ctl_IR_in <= 0;
    ctl_GP_in <= 0; ctl_GP_out <= 0; ctl_GP_sel <= 4'b0;
    ctl_HILO_in <= 0; ctl_HI_out <= 0; ctl_LO_out <= 0;
    ctl_Y_in <= 0; ctl_Y_out <= 0;
    ctl_Z_in <= 0; ctl_Z_out <= 0;
    ctl_MAR_in <= 0;
    ctl_MDR_in <= 0; ctl_MDR_out <= 0;
    ctl_mem_read <= 0;
    ctl_alu_and <= 0; ctl_alu_or <= 0; ctl_alu_shr <= 0; ctl_alu_shra <= 0; ctl_alu_shl <= 0; ctl_alu_ror <= 0; ctl_alu_rol <= 0;
    ctl_alu_neg <= 0; ctl_alu_not <= 0; ctl_alu_add <= 0; ctl_alu_sub <= 0; ctl_alu_mul <= 0; ctl_alu_div <= 0; ctl_alu_incpc <= 0;
    mem_data_in <= 32'b0;
    case (Present_state)
        Reg_load1a: begin
            mem_data_in <= ra_val;
            ctl_mem_read <= 1; ctl_MDR_in <= 1;
        end
        Reg_load1b: begin
            ctl_MDR_out <= 1; ctl_GP_sel <= IR_a; ctl_GP_in <= 1;
            mem_data_in <= rb_val;
            ctl_mem_read <= 1; ctl_MDR_in <= 1;
        end
        Reg_load2b: begin
            ctl_MDR_out <= 1; ctl_GP_sel <= IR_b; ctl_GP_in <= 1;
            mem_data_in <= rc_val;
            ctl_mem_read <= 1; ctl_MDR_in <= 1;
        end
        Reg_load3b: begin
            ctl_MDR_out <= 1; ctl_GP_sel <= IR_c; ctl_GP_in <= 1;
        end
        T0: begin
            ctl_PC_out <= 1; ctl_MAR_in <= 1; ctl_alu_incpc <= 1; ctl_Z_in <= 1;
        end
        T1: begin
            ctl_Z_out <= 1; ctl_PC_in <= 1; ctl_mem_read <= 1; ctl_MDR_in <= 1;
            mem_data_in <= IR;
        end
        T2: begin
            ctl_MDR_out <= 1; ctl_IR_in <= 1;
        end
        T3: begin
            ctl_GP_sel <= (IR_mul || IR_div) ? IR_a : IR_b; ctl_GP_out <= 1; ctl_Y_in <= 1;
        end
        T4: begin
            ctl_GP_sel <= (IR_mul || IR_div || IR_not || IR_neg) ? IR_b : IR_c; ctl_GP_out <= 1;

            // which ALU op are we doing?
            ctl_alu_add <= IR_add;
            ctl_alu_sub <= IR_sub;
            ctl_alu_and <= IR_and;
            ctl_alu_or <= IR_or;
            ctl_alu_shr <= IR_shr;
            ctl_alu_shra <= IR_shra;
            ctl_alu_shl <= IR_shl;
            ctl_alu_ror <= IR_ror;
            ctl_alu_rol <= IR_rol;
            ctl_alu_mul <= IR_mul;
            ctl_alu_div <= IR_div;
            ctl_alu_neg <= IR_neg;
            ctl_alu_not <= IR_not;

            // load Z with the result
            ctl_Z_in <= 1;
            if (IR_mul || IR_div) begin
                // if we're doing MUL or DIV, put the result directly into HI/LO
                ctl_HILO_in <= 1;
            end
        end
        T5: begin
            if (IR_mul || IR_div) begin
                // do nothing?
            end else begin
                // move result from Z into GP reg
                ctl_Z_out <= 1; ctl_GP_sel <= IR_a; ctl_GP_in <= 1;
            end
        end
        Reg_check1: begin
            ctl_MDR_in <= 1;
            if (IR_mul || IR_div) begin
                // move low word of result into MDR
                ctl_LO_out <= 1;
            end else begin
                // move result from GP reg into MDR
                ctl_GP_sel <= IR_a; ctl_GP_out <= 1;
            end
        end
        Reg_check2: begin
        
            if (IR_mul || IR_div) begin
                ctl_HI_out <= 1;
                ctl_MDR_in <= 1;
            end
            
            // do actual arithmetic check (low word for MUL/DIV)
            expected_value = arithmetic_out[31:0];
            #1 $display("[31:0]  Expected Value: %08x   Actual Value: %08x", expected_value, mem_data_out);
            case(mem_data_out)
                expected_value: $display("Same!");
                default: $error("ALU output mismatch");
            endcase
        end
        Reg_check3: begin
            if (IR_mul || IR_div) begin
                // do actual arithmetic check (high word for MUL/DIV)
                expected_value = arithmetic_out[63:32];
                #1 $display("[63:32] Expected Value: %08x   Actual Value: %08x", expected_value, mem_data_out);
                case(mem_data_out)
                    expected_value: $display("Same!");
                    default: $error("ALU output mismatch");
                endcase
            end
        end
    endcase
end

endmodule
