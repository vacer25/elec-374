onerror {resume}
quietly WaveActivateNextPane {} 0
delete wave *
add wave -noupdate /phase1_tb/clk
add wave -noupdate /phase1_tb/reset
add wave -noupdate /phase1_tb/ctl_GP_in
add wave -noupdate /phase1_tb/ctl_GP_out
add wave -noupdate -radix unsigned /phase1_tb/ctl_GP_sel
add wave -noupdate /phase1_tb/ctl_Y_in
add wave -noupdate /phase1_tb/ctl_Y_out
add wave -noupdate /phase1_tb/ctl_Z_in
add wave -noupdate /phase1_tb/ctl_ZL_out
add wave -noupdate /phase1_tb/ctl_ZH_out
add wave -noupdate -radix hexadecimal /phase1_tb/mem_data_in
add wave -noupdate -radix unsigned /phase1_tb/Present_state
add wave -noupdate -radix hexadecimal /phase1_tb/dut/bus
add wave -noupdate -radix hexadecimal /phase1_tb/dut/MDR/q
add wave -noupdate -radix hexadecimal /phase1_tb/dut/ALU/A
add wave -noupdate -radix hexadecimal /phase1_tb/dut/ALU/B
add wave -noupdate -radix hexadecimal /phase1_tb/dut/ALU/C
add wave -noupdate -radix hexadecimal /phase1_tb/dut/Z/q
add wave -noupdate -radix hexadecimal /phase1_tb/dut/HI/q
add wave -noupdate -radix hexadecimal /phase1_tb/dut/LO/q
add wave -noupdate -radix hexadecimal {/phase1_tb/dut/GP/register_file_loop[0]/current_gp_register/q}
add wave -noupdate -radix hexadecimal {/phase1_tb/dut/GP/register_file_loop[1]/current_gp_register/q}
add wave -noupdate -radix hexadecimal {/phase1_tb/dut/GP/register_file_loop[2]/current_gp_register/q}
add wave -noupdate -radix hexadecimal {/phase1_tb/dut/GP/register_file_loop[3]/current_gp_register/q}
add wave -noupdate -radix hexadecimal {/phase1_tb/dut/GP/register_file_loop[4]/current_gp_register/q}
add wave -noupdate -radix hexadecimal {/phase1_tb/dut/GP/register_file_loop[5]/current_gp_register/q}
add wave -noupdate -radix hexadecimal {/phase1_tb/dut/GP/register_file_loop[6]/current_gp_register/q}
add wave -noupdate -radix hexadecimal {/phase1_tb/dut/GP/register_file_loop[7]/current_gp_register/q}
add wave -noupdate -radix hexadecimal {/phase1_tb/dut/GP/register_file_loop[8]/current_gp_register/q}
add wave -noupdate -radix hexadecimal {/phase1_tb/dut/GP/register_file_loop[9]/current_gp_register/q}
add wave -noupdate -radix hexadecimal {/phase1_tb/dut/GP/register_file_loop[10]/current_gp_register/q}
add wave -noupdate -radix hexadecimal {/phase1_tb/dut/GP/register_file_loop[11]/current_gp_register/q}
add wave -noupdate -radix hexadecimal {/phase1_tb/dut/GP/register_file_loop[12]/current_gp_register/q}
add wave -noupdate -radix hexadecimal {/phase1_tb/dut/GP/register_file_loop[13]/current_gp_register/q}
add wave -noupdate -radix hexadecimal {/phase1_tb/dut/GP/register_file_loop[14]/current_gp_register/q}
add wave -noupdate -radix hexadecimal {/phase1_tb/dut/GP/register_file_loop[15]/current_gp_register/q}
restart -force
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 258
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ps} {200 ns}
run 200ns