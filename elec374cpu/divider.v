// implemented 2019-01-27
module divider (input signed [31:0] a, b, output [63:0] c);
    assign c[63:32] = a / b;
    assign c[31:0] = a % b;
endmodule
