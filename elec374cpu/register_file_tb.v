// This module is intended for the HDLBits online simulator, which can
// be found at https://hdlbits.01xz.net/wiki/Iverilog
// Concatenate this file with all other required files and paste into
// the code window.
// implemented 2019-01-23
module top_module();
    initial `probe_start;   // Start the timing diagram
    
    reg clk=0;
    always #5 clk = ~clk;  // Create clock with period=10
    `probe(clk);        // Probe signal "clk"

    // A testbench
    reg reset_all, reset_one, wr;
    reg  [2:0] addr;
    reg  [7:0] d;
    wire [7:0] q;
    `probe(reset_all);
    `probe(reset_one);
    `probe(wr);
    `probe(addr);
    `probe(d);
    `probe(q);
    initial begin
        reset_one = 0; reset_all = 1; #20
        reset_all = 0; addr = 0; d = 8'h32; wr = 0; #20
        wr = 1; #20
        wr = 0; #20
        addr = 1; #20
        addr = 0; #20
        #50 $finish;            // Quit the simulation
    end
    
    register_file #(.WIDTH(8), .COUNT(16)) regs(
        .clk(clk), .reset_all(reset_all), .reset_one(reset_one), .wr(wr),
        .addr(addr), .d(d), .q(q));
endmodule
