// implemented 2019-01-25
module left_shifter (
    input [31:0] in,
    input [4:0] amt,
    output [31:0] out
);
    // simple barrel shifter
    wire [31:0] im0 = amt[0] ? {in[30:0], 1'b0} : in;
    wire [31:0] im1 = amt[1] ? {im0[29:0], 2'b0} : im0;
    wire [31:0] im2 = amt[2] ? {im1[27:0], 4'b0} : im1;
    wire [31:0] im3 = amt[3] ? {im2[23:0], 8'b0} : im2;
    wire [31:0] im4 = amt[4] ? {im3[15:0], 16'b0} : im3;
    assign out = im4;
endmodule
