// implemented 2019-01-23
module alu (
    input ctl_and, ctl_or, ctl_shr, ctl_shra, ctl_shl, ctl_ror, ctl_rol,
        ctl_neg, ctl_not, ctl_add, ctl_sub, ctl_mul, ctl_div, ctl_incpc,
    input [31:0] A, input [31:0] B,
    output [63:0] C
);
    // Derived general control lines
    wire ctl_add_p = ctl_add | ctl_sub | ctl_neg | ctl_not | ctl_incpc;  // Adder is used for addition, subtraction, negation, not operation, and PC increment.
    wire ctl_shr_p = ctl_ror | ctl_rol | ctl_shr | ctl_shra; // Right shift block is used for both logical and arithmetic right shifts, as well as both right and left rotations

    // Intermediate results
    wire [31:0] and_calc;
    wire [31:0] or_calc;
    wire [31:0] shl_calc;
    wire [31:0] shr_calc;
    wire [31:0] adder_calc;
    wire [63:0] mul_calc;    // Multiply can produce 64 bit numbers
    wire [63:0] div_calc;    // Divider can produce 64 bit numbers

    // Result selector based on control signal
    // TODO: Research how to deactivate the circuitry when it's not in use?
    mux_onehot #(.WIDTH(32), .COUNT(7)) alu_mux(
        .in({and_calc, or_calc, shl_calc, shr_calc, adder_calc, mul_calc[31:0], div_calc[31:0]}),
        .sel({ctl_and, ctl_or, ctl_shl, ctl_shr_p, ctl_add_p, ctl_mul, ctl_div}),
        .out(C[31:0])
    );
    mux_onehot #(.WIDTH(32), .COUNT(2)) alu_muxhigh(
        .in({mul_calc[63:32],div_calc[63:32]}),
        .sel({ctl_mul,ctl_div}),
        .out(C[63:32])
    );

    // Simple logical operators
    assign and_calc = A & B;
    assign or_calc = A | B;
    
    // Left shift
    left_shifter alu_lshift(
        .in(A), .amt(B[4:0]),
        .out(shl_calc)
    );
    
    // Right shifts / right and left rotates
    // x rol n = x ror (32 - n)
    // Since n is 5 bits, it's mod 32
    // (32 - n) is congruent to -n, mod 32
    wire [4:0] shr_amt = ctl_rol ? -B[4:0] : B[4:0];
    wire shr_do_rotate = ctl_rol || ctl_ror;
    wire shr_fill_bit = ctl_shra ? A[31] : 1'b0;
    right_shifter alu_rshift(
        .in(A), .amt(shr_amt), .rotate(shr_do_rotate), .fill_bit(shr_fill_bit),
        .out(shr_calc)
    );

    // Adder instantiation and additional derived control lines
    wire ctl_add_lhs_zero = ctl_not || ctl_neg;         // The left hand side is zero when negating or doing not operation
    wire ctl_add_inverted = ctl_not || ctl_neg || ctl_sub; // Used to determine if right hand side needs to be inverted
    wire [31:0] add_a = ctl_incpc ? 32'd4 : (ctl_add_lhs_zero ? 32'b0 : A);   // Make the left hand side zero when it needs to be
    wire [31:0] add_b = ctl_add_inverted ? ~B : B;      // Make the right hand side inverted when it needs to be
    wire ctl_add_carry_in = ctl_sub || ctl_neg;         // There must be a carry in of 1 when doing subtraction or negation
    adder alu_adder(
        .a(add_a), .b(add_b), .carry_in(ctl_add_carry_in),
        .c(adder_calc)
    );

    // Instantiate multiplier and divider
    multiplier alu_mul(
        .a(A), .b(B),
        .c(mul_calc)
    );
    
    divider alu_div(
        .a(A), .b(B),
        .c(div_calc)
    );
    
endmodule

// C: and
// C: or
// shr
// shra
// shl
// ror
// rol
// C: neg
// C: not
// add
// C: sub
// mul
// div
