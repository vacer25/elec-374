// implemented 2019-01-23
module register_file #(parameter WIDTH = 32, COUNT = 16) (
    input clk, input reset, input wr,
    input [$clog2(COUNT) - 1 : 0] addr,
    input [WIDTH - 1 : 0] d,
    output [WIDTH - 1 : 0] q
);

    // Control lines for selecting active register
    wire [COUNT - 1 : 0] sel;
    binary_to_onehot #(.SIZE(COUNT)) decoder(.binary(addr), .onehot(sel));
    
    // Control lines for selecting which register to reset and which register to write to
    wire [COUNT - 1 : 0] sel_wr = sel & {COUNT{wr}};

    // TODO: explain how this works
    wire [WIDTH * COUNT - 1 : 0] bigvec;
    
    // Generate the general purpose registers
    generate
        genvar i;
        for (i = 0; i < COUNT; i = i + 1) begin : register_file_loop
            register #(.WIDTH(WIDTH)) current_gp_register(
                .clk(clk), .reset(reset), .wr(sel_wr[i]),
                .d(d),
                .q(bigvec[(i+1) * WIDTH - 1 : i * WIDTH])
            );
        end
    endgenerate
    
    // TODO: explain how this works
    mux_onehot #(.WIDTH(WIDTH), .COUNT(COUNT)) mux_out(.in(bigvec), .out(q), .sel(sel));
endmodule
