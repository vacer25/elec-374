// implemented 2019-01-23
// as of testing on 2019-01-28, has 3700 usage
module mux_onehot #(parameter WIDTH = 32, COUNT = 8) (
    input [WIDTH * COUNT - 1 : 0] in,
    input [COUNT - 1 : 0] sel,
    output [WIDTH - 1 : 0] out
);
    // Using ternary operator chain -> priority encoder
    generate
        genvar i;
        for (i = 0; i < COUNT; i = i + 1) begin : mux_onehot_loop
            assign out = sel[i] ? in[(i+1) * WIDTH - 1 : i * WIDTH] : {WIDTH{1'bz}};
        end
    endgenerate
endmodule

// // implemented 2019-01-28
// // as of testing on 2019-01-28, has 3754 usage
// module mux_onehot #(parameter WIDTH = 32, COUNT = 8) (
//     input [WIDTH * COUNT - 1 : 0] in,
//     input [COUNT - 1 : 0] sel,
//     output [WIDTH - 1 : 0] out
// );
//     // Using loop-ifs as case statement replacement to form priority encoder
//     reg [WIDTH - 1 : 0] out_buf;
//     assign out = out_buf;
    
//     wire [WIDTH - 1 : 0] arr [COUNT - 1 : 0];
//     generate
//         genvar i;
//         for (i = 0; i < COUNT; i = i + 1) begin : mux_onehot_loop
//             assign arr[i] = in[(i+1) * WIDTH - 1 : i * WIDTH];
//         end
//     endgenerate
    
//     integer j;
//     always @(*) begin
//         out_buf = {WIDTH{1'bx}};
//         for(j = 0; j < COUNT; j = j + 1) begin
//             if (sel[j]) out_buf = arr[j];
//         end
//     end
// endmodule
