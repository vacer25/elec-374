// implementation 2019-01-29
// based on: http://vlsiip.com/download/booth.pdf
// 5910 resources used
module multiplier (
    input signed [31:0] a, input signed [31:0] b,
    output signed [63:0] c
);

    wire signed [31:0] neg_a;					// Negative (2's compilent) of multiplicand a
    reg [2:0] combinedBits [15:0]; 				// Groups of 3 bits of multiplier b
    reg signed [32:0] partialProducts [15:0];  		// The partial products which are computed based on combinedBits
    reg signed [63:0] shiftedPartialProducts [15:0];	// The partial products shifted to the correct amount, in preparation to add them
    reg signed [63:0] sumPartialProducts;		// The sum of the partial products, which is the answer in the end
    integer i, j;								// Loop indexes
    
    // Generate two’s compiment of multiplicand a
    assign neg_a = -a;//  For some reason ~a + 1 does not work!
    
    // Compute producto when a (or neg_a) or b changes
    always @ (a or b or neg_a)
    begin

        //$display("a = %b", a);
        //$display("a = %d", a);
        //$display("b = %b", b);
        //$display("b = %d", b);
        //$display("neg_a = %b", neg_a);
        //$display("neg_a = %d", neg_a);
    
        // Determine combinedBits from multiplier b
        
        // Special case: LSB is zero since b[-1] does not exist
        combinedBits[0] = {b[1], b[0], 1'b0};
        //$display("cb = %b", combinedBits[0]);
            
        // For the rest of the bits in b
        // Loop over all bits in b but take every 2nd bit as the current center bit
        for(i=1;i<16;i=i+1) begin
            // Concatenate the 3 neighboring bits around the current bit
                combinedBits[i] = {b[2*i+1],b[2*i],b[2*i-1]};
            //$display("cb = %b", combinedBits[i]);
        end
        
        // Compute all the partial products
        for(i=0;i<16;i=i+1)	begin
            
            // The computation of the current partial product depends on the current combinedBits of b
            case(combinedBits[i])
                
                // If bits are 001 or 010, bit recoding is +1, partial product = a
                3'b001 , 3'b010 : partialProducts[i] = {a[31],a};
                // If bits are 001, bit recoding is +1, partial product = 2*a (add 0 on left to shift right by 1)
                3'b011 : partialProducts[i] = {a,1'b0};
                // If bits are 100, bit recoding is -1, partial product = -2*a (negate and add 0 on left to shift right by 1)
                3'b100 : partialProducts[i] = {neg_a[31:0],1'b0};
                // If bits are 101 or 110, bit recoding is -1, partial product = -a
                3'b101 , 3'b110 : partialProducts[i] = neg_a;
                
                // The default case should never happen
                default : partialProducts[i] = 0;
                
            endcase
            
            //$display("pp =  %b", partialProducts[i]);
            
            // Sign extend each partial product
            shiftedPartialProducts[i] = partialProducts[i] << (2*i);
            
            //$display("spp = %b", shiftedPartialProducts[i]);
            
        end
        
        // Start adding up the partial products with the first one
        sumPartialProducts = shiftedPartialProducts[0];
        
        // Add up all the remaining partial products together
        for(i=1;i<16;i=i+1) begin
            sumPartialProducts = sumPartialProducts + shiftedPartialProducts[i];
        end
        
        //$display("Ans = %b", sumPartialProducts);
        //$display("Ans = %d", sumPartialProducts);
        
    end
    
    // The output is the computed product
    assign c = sumPartialProducts;

endmodule

/*

// iverilog testbench

module top_module ();
    reg clk=0;
    always #5 clk = ~clk;  // Create clock with period=10
    initial `probe_start;   // Start the timing diagram

    `probe(clk);        // Probe signal "clk"

    // A testbench
    wire [31:0] a = -578;
    wire [31:0] b = 467;
    wire [63:0] c = 1;
    
    initial begin
        #10;
        #50 $finish;            // Quit the simulation
    end

    multiplier mul (.a(a), .b(b), .c(c));

endmodule

module multiplier (input wire signed [31:0] a, input wire signed [31:0] b, output wire signed [63:0] c);
    
    wire signed [31:0] neg_a;					// Negative (2's compilent) of multiplicand a
    reg [2:0] combinedBits [15:0]; 				// Groups of 3 bits of multiplier b
    reg [31:0] partialProducts [15:0];  		// The partial products which are computed based on combinedBits
    reg [63:0] shiftedPartialProducts [15:0];	// The partial products shifted to the correct amount, in preparation to add them
    reg signed [63:0] sumPartialProducts;		// The sum of the partial products, which is the answer in the end
    integer i, j;								// Loop indexes
    
    // Generate two’s compiment of multiplicand a
    assign neg_a = -a;//  For some reason ~a + 1 does not work!
    
    // Compute producto when a (or neg_a) or b changes
    always @ (a or b or neg_a)
    begin

        $display("a = %b", a);
        $display("a = %d", a);
        $display("b = %b", b);
        $display("b = %d", b);
        $display("neg_a = %b", neg_a);
        $display("neg_a = %d", neg_a);
    
        // Determine combinedBits from multiplier b
        
        // Special case: LSB is zero since b[-1] does not exist
        combinedBits[0] = {b[1], b[0], 1'b0};
        $display("cb = %b", combinedBits[0]);
            
        // For the rest of the bits in b
        // Loop over all bits in b but take every 2nd bit as the current center bit
        for(i=1;i<16;i=i+1) begin
            // Concatenate the 3 neighboring bits around the current bit
            combinedBits[i] = {b[2*i+1],b[2*i],b[2*i-1]};
            $display("cb = %b", combinedBits[i]);
        end
        
        // Compute all the partial products
        for(i=0;i<16;i=i+1)	begin
            
            // The computation of the current partial product depends on the current combinedBits of b
            case(combinedBits[i])
                
                // If bits are 001 or 010, bit recoding is +1, partial product = a
                3'b001 , 3'b010 : partialProducts[i] = {a[31],a};
                // If bits are 001, bit recoding is +1, partial product = 2*a (add 0 on left to shift right by 1)
                3'b011 : partialProducts[i] = {a,1'b0};
                // If bits are 100, bit recoding is -1, partial product = -2*a (negate and add 0 on left to shift right by 1)
                3'b100 : partialProducts[i] = {neg_a[31:0],1'b0};
                // If bits are 101 or 110, bit recoding is -1, partial product = -a
                3'b101 , 3'b110 : partialProducts[i] = neg_a;
                
                // The default case should never happen
                default : partialProducts[i] = 0;
                
            endcase
            
            //$display("pp =  %b", partialProducts[i]);
            
            // Sign extend each partial product
            shiftedPartialProducts[i] = $signed(partialProducts[i]);
            
            // Shift each partial product right by required amount by adding 00 on left
            for(j=0;j<i;j=j+1) begin
                shiftedPartialProducts[i] = {shiftedPartialProducts[i],2'b00};
            end
            
            $display("spp = %b", shiftedPartialProducts[i]);
            
        end
        
        // Start adding up the partial products with the first one
        sumPartialProducts = shiftedPartialProducts[0];
        
        // Add up all the remaining partial products together
        for(i=1;i<16;i=i+1) begin
            sumPartialProducts = sumPartialProducts + shiftedPartialProducts[i];
        end
        
        $display("Ans = %b", sumPartialProducts);
        $display("Ans = %d", sumPartialProducts);
        
    end
    
    // The output is the computed product
    assign c = sumPartialProducts;
    

    `probe(a);
    `probe(b);
    `probe(c);
    
    `probe(neg_a);
    
endmodule

*/

/* Cooper's multipliers

// 6264 resources used
module mult_booth1(input [31:0] a, b, output [63:0] c);
    wire [63:0] p [15:0];
    assign p[0] = 0;
    assign p[1] = {{32{b[31]}}, b};
    assign p[2] = p[1];
    assign p[3] = {{31{b[31]}}, b, 1'b0};
    assign p[4] = p[3];
    assign p[5] = {{31{b[31]}}, b, 1'b0} + {{32{b[31]}}, b};
    assign p[6] = p[5];
    assign p[7] = {{30{b[31]}}, b, 2'b0};
    generate
        genvar i;
        for (i = 8; i < 16; i = i + 1) assign p[i] = -p[7 + 8 - i];
    endgenerate
    assign c =
        p[{a[2:0], 1'b0}] + 
        {p[a[5:2]][61:0], 3'b0} +
        {p[a[8:5]][58:0], 6'b0} +
        {p[a[11:8]][55:0], 9'b0}+
        {p[a[14:11]][52:0], 12'b0}+
        {p[a[17:14]][49:0], 15'b0}+
        {p[a[20:17]][46:0], 18'b0}+
        {p[a[23:20]][43:0], 21'b0}+
        {p[a[26:23]][40:0], 24'b0}+
        {p[a[29:26]][37:0], 27'b0}+
        {p[{1'b0, a[31:29]}][34:0], 30'b0};
endmodule

// 5948 resources used
module mult_booth2(input signed [31:0] a, b, output [63:0] c);
    wire signed [32:0] p [7:0];

    // Get potential choices selected by 3-digit group
    assign p[0] = 0;
    assign p[1] = {b[31], b};
    assign p[2] = p[1];
    assign p[3] = {b, 1'b0};
    // Upper half is equal to the negative of the corresponding lower half
    generate
        genvar i;
        for (i = 0; i < 4; i = i + 1) assign p[7 - i] = -p[i];
    endgenerate

    // Divide up A into pairs + prev. bit
    wire [2:0] selector [15:0];
    // First group has no prev. bit -> 0
    assign selector[0] = {a[1:0], 1'b0};
    generate
        genvar j;
        for(j = 1; j < 16; j = j + 1) begin
            assign selector[j] = a[(2*j + 1):(2*j - 1)];
        end
    endgenerate
    
    // Select which choice to add based on digits, and shift it by appropriate amount
    wire [63:0] q [15:0];
    
    reg [63:0] result;
    reg [63:0] tmp;
    assign c = result;
    integer k;
    always @(*) begin
        result = 0;
        for(k = 0; k < 16; k = k + 1) begin
            tmp = (p[selector[k]]) << (k * 2);
            result = result + tmp;
        end
    end
endmodule

*/
