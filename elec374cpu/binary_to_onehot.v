// implemented 2019-01-23
module binary_to_onehot #(parameter SIZE = 4) (
    input [$clog2(SIZE) - 1 : 0] binary,
    output [SIZE - 1 : 0] onehot
);
    
    // Set each bit of the output if that bit position is set in the binary
    //   input to implement onehot logic
    generate
        genvar i;
        for (i = 0; i < SIZE; i = i + 1) begin : binary_to_onehot_loop
            assign onehot[i] = (binary == i);
        end
    endgenerate
endmodule
