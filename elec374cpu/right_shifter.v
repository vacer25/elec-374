// implemented 2019-01-25
module right_shifter (
    input [31:0] in,
    input [4:0] amt,
    input rotate, fill_bit,
    output [31:0] out
);
    // simple barrel shifter
    wire shift = !rotate;
    wire [31:0] im0 = amt[0] ? {shift ? { 1{fill_bit}} : in[0], in[31:1]} : in;
    wire [31:0] im1 = amt[1] ? {shift ? { 2{fill_bit}} : im0[1:0], im0[31:2]} : im0;
    wire [31:0] im2 = amt[2] ? {shift ? { 4{fill_bit}} : im1[3:0], im1[31:4]} : im1;
    wire [31:0] im3 = amt[3] ? {shift ? { 8{fill_bit}} : im2[7:0], im2[31:8]} : im2;
    wire [31:0] im4 = amt[4] ? {shift ? {16{fill_bit}} : im3[15:0], im3[31:16]} : im3;
    assign out = im4;
endmodule
