// implemented 2019-01-23
module register #(parameter WIDTH = 32) (
    input clk, input reset, input wr,
    input [WIDTH - 1:0] d,
    output reg [WIDTH - 1:0] q
);

    // Reset when asyncronous reset is applied
    // BUGFIX 2019-01-27: Incorrect write behaviour
    //   The prev. behaviour would write if:
    //   1.  wr == 1 and posedge clk
    //       --or--
    //   2.  clk == 1 and posedge wr
    //   due to the use of the 'and' before the sensitivity list. By moving
    //   the 'wr' check inside the always block, we eliminate the erroneous
    //   write in scenario 2.
    always @(posedge clk, posedge reset)
        if (reset) q <= {WIDTH{1'b0}};
        else if (wr) q <= d;
endmodule
