// implemented 2019-01-23
module datapath (
    input clk, reset,

    // Visible register control interfaces
    input ctl_PC_in, ctl_PC_out,
    input ctl_IR_in,
    input ctl_GP_in, ctl_GP_out, input [3:0] ctl_GP_sel,
    input ctl_HILO_in, ctl_HI_out, ctl_LO_out,

    // Internal register control interfaces
    input ctl_Y_in, ctl_Y_out,
    input ctl_Z_in, ctl_Z_out,
    input ctl_MAR_in,
    input ctl_MDR_in, ctl_MDR_out,

    // Leftover from given testbench
    // TODO: sort out this stuff later
    input ctl_mem_read,
    input ctl_alu_and, ctl_alu_or, ctl_alu_shr, ctl_alu_shra, ctl_alu_shl, ctl_alu_ror, ctl_alu_rol,
    input ctl_alu_neg, ctl_alu_not, ctl_alu_add, ctl_alu_sub, ctl_alu_mul, ctl_alu_div, ctl_alu_incpc,
    input [31:0] mem_data_in,
    output [31:0] mem_data_out
);
    // Let's get bus stuff out of the way
    wire [31:0] bus;
    wire [63:0] alu_out;
    
    // PC register definition
    wire [31:0] PC_in;
    wire [31:0] PC_out;
    register #(.WIDTH(32)) PC (
        .clk(clk), .reset(reset), .wr(ctl_PC_in),
        .d(PC_in), .q(PC_out)
    );
    assign PC_in = bus;
    
    // IR definition
    wire [31:0] IR_in;
    wire [31:0] IR_out;
    register #(.WIDTH(32)) IR (
        .clk(clk), .reset(reset), .wr(ctl_IR_in),
        .d(IR_in), .q(IR_out)
    );
    assign IR_in = bus;
    
    // GP register file definition
    wire [31:0] GP_in;
    wire [31:0] GP_out;
    register_file #(.WIDTH(32), .COUNT(16)) GP (
        .clk(clk), .reset(reset), .wr(ctl_GP_in),
        .addr(ctl_GP_sel),
        .d(GP_in), .q(GP_out)
    );
    assign GP_in = bus;
    
    // HI register definition
    wire [31:0] HI_in;
    wire [31:0] HI_out;
    register #(.WIDTH(32)) HI (
        .clk(clk), .reset(reset), .wr(ctl_HILO_in),
        .d(HI_in), .q(HI_out)
    );
    assign HI_in = alu_out[63:32];
    
    // LO register definition
    wire [31:0] LO_in;
    wire [31:0] LO_out;
    register #(.WIDTH(32)) LO (
        .clk(clk), .reset(reset), .wr(ctl_HILO_in),
        .d(LO_in), .q(LO_out)
    );
    assign LO_in = alu_out[31:0];
    
    // Y register definition
    wire [31:0] Y_in;
    wire [31:0] Y_out;
    register #(.WIDTH(32)) Y (
        .clk(clk), .reset(reset), .wr(ctl_Y_in),
        .d(Y_in), .q(Y_out)
    );
    assign Y_in = bus;
    
    // Z register definition
    wire [31:0] Z_in;
    wire [31:0] Z_out;
    register #(.WIDTH(32)) Z (
        .clk(clk), .reset(reset), .wr(ctl_Z_in),
        .d(Z_in), .q(Z_out)
    );
    assign Z_in = alu_out[31:0];
    
    // MAR definition
    wire [31:0] MAR_in;
    wire [31:0] MAR_out;
    register #(.WIDTH(32)) MAR (
        .clk(clk), .reset(reset), .wr(ctl_MAR_in),
        .d(MAR_in), .q(MAR_out)
    );
    assign MAR_in = bus;
    
    // MDR definition
    wire [31:0] MDR_in;
    wire [31:0] MDR_out;
    register #(.WIDTH(32)) MDR (
        .clk(clk), .reset(reset), .wr(ctl_MDR_in),
        .d(MDR_in), .q(MDR_out)
    );
    assign MDR_in = ctl_mem_read ? mem_data_in : bus;
    assign mem_data_out = MDR_out;

    // Instantiation of various bits
    // TODO: implement all arithmetic operations and add control lines for them here
    alu ALU(
        .ctl_and(ctl_alu_and), .ctl_or(ctl_alu_or), .ctl_shr(ctl_alu_shr), .ctl_shra(ctl_alu_shra), .ctl_shl(ctl_alu_shl), .ctl_ror(ctl_alu_ror), .ctl_rol(ctl_alu_rol),
        .ctl_neg(ctl_alu_neg), .ctl_not(ctl_alu_not), .ctl_add(ctl_alu_add), .ctl_sub(ctl_alu_sub), .ctl_mul(ctl_alu_mul), .ctl_div(ctl_alu_div), .ctl_incpc(ctl_alu_incpc),
        .A(Y_out), .B(bus),
        .C(alu_out)
    );
    
    // Remember to change count if you change the inputs of the multiplexer!!!
    mux_onehot #(.WIDTH(32), .COUNT(7)) bus_mux(
        .sel({ctl_PC_out, ctl_GP_out, ctl_HI_out, ctl_LO_out, ctl_Y_out, ctl_Z_out, ctl_MDR_out}),
        .in({PC_out, GP_out, HI_out, LO_out, Y_out, Z_out, MDR_out}),
        .out(bus)
    );
endmodule
