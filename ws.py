#!/usr/bin/env python3
import sys

tw = 4

def linefunc(s):
    so = ''
    i = 0
    count = 0
    for c in s:
        if c == '\t':
            count += (tw - (count % tw))
        elif c == ' ':
            count += 1
        else:
            break
        i += 1
    if count % tw < tw / 2:
        count -= count % tw
    else:
        count += tw - (count % tw)
    return ' ' * count + s[i:]

def main(args):
    if len(args) == 1:
        sys.stdout.writelines((linefunc(x) for x in sys.stdin))
        return 0
    if len(args) < 3:
        print('Usage: {} <input> <output>'.format(args[0]))
        return 1
    p_in = args[1]
    p_out = args[2]
    with open(p_in, 'r', newline = '') as f_in, open(p_out, 'x', newline = '') as f_out:
        f_out.writelines((linefunc(x) for x in f_in))

if __name__ == "__main__":
    sys.exit(main(sys.argv))
