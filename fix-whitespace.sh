#!/bin/sh
DIRPATH="$(dirname '$0')"
FILES="$(find $DIRPATH/elec374cpu/ -name '*.v')"
for x in $FILES; do
    mv "$x" "$x.bak" && ./ws.py "$x.bak" "$x" && rm "$x.bak"
done
